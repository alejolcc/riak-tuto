defmodule RiakCoreTutorial.MixProject do
  use Mix.Project

  def project do
    [
      app: :riak_core_tutorial,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:riak_core, :logger],
      mod: {RiakCoreTutorial.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # riak_core dependencies
      {:riak_core, github: "Kyorai/riak_core", branch: "fifo-merge"},
      {:cuttlefish,
        github: "rabbitmq/cuttlefish", branch: "develop", manager: :rebar3, override: true},
      {:goldrush, github: "DeadZen/goldrush", tag: "0.1.9", manager: :rebar3, override: true},
      {:lager, github: "erlang-lager/lager", override: true},
    ]
  end
end
