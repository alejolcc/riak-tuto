defmodule RiakCoreTutorial do
  @moduledoc """
  Documentation for RiakCoreTutorial.
  """

  @doc """
  Hello world.

  ## Examples

      iex> RiakCoreTutorial.hello()
      :world

  """
  def hello do
    :world
  end
end
