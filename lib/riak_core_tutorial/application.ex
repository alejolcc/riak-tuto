defmodule RiakCoreTutorial.Application do
  use Application
  require Logger

  def start(_type, _args) do
    case RiakCoreTutorial.Supervisor.start_link do
      {:ok, pid} ->
        :ok = :riak_core.register(vnode_module: RiakCoreTutorial.VNode)
        :ok = :riak_core_node_watcher.service_up(RiakCoreTutorial.Service, self())
        {:ok, pid}
      {:error, reason} ->
        Logger.error("Unable to start RiakCoreTutorial supervisor because: #{inspect reason}")
    end
  end

end