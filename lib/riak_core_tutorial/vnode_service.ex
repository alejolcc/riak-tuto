defmodule RiakCoreTutorial.Service do

  def ping(v\\1) do
    idx = :riak_core_util.chash_key({"riak_tutorial", "ping#{v}"})
    pref_list = :riak_core_apl.get_primary_apl(idx, 1, RiakCoreTutorial.Service)

    [{index_node, _type}] = pref_list

    :riak_core_vnode_master.sync_command(index_node, {:ping, v}, RiakCoreTutorial.VNode_master)
  end

  def put(k, v) do
    idx = :riak_core_util.chash_key({"noslides", k})
    pref_list = :riak_core_apl.get_primary_apl(idx, 1, RiakCoreTutorial.Service)

    [{index_node, _type}] = pref_list

    :riak_core_vnode_master.command(index_node, {:put, {k, v}}, RiakCoreTutorial.VNode_master) 
  end

  def get(k) do
    idx = :riak_core_util.chash_key({"noslides", k})
    pref_list = :riak_core_apl.get_primary_apl(idx, 1, RiakCoreTutorial.Service)

    [{index_node, _type}] = pref_list

    :riak_core_vnode_master.sync_command(index_node, {:get, k}, RiakCoreTutorial.VNode_master)
  end
end